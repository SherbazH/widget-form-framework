export default interface IConfig {
  serviceUrl: string;
  dataSource?: string;
}