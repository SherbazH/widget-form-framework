# ArcGIS Web AppBuilder Widget Form Framework

This project should be the foundation for a ArcGIS WebAppBuilder widget with dynamic form loading requirements. 

## Development 

In order to develop this further, you require an installation of both ArcGIS WebAppBuilder & a WebAppBuilder TypeScript Compiler as described in [the compiler repository]('https://bitbucket.org/flexedge/wab-compiler/src'). Follow the README there to ensure this works as intended.

## Deployment

To deploy, add this widget to the WebAppBuilder Application you seek to deploy, and follow the standard WebAppBuilder [deployment protocol]('https://developers.arcgis.com/web-appbuilder/guide/xt-deploy-app.htm').


## Further Questions
For any further questions feel free to contact:

*Sherbaz Hashmi* 
**sherbaz.hashmi@broadspectrum.com**