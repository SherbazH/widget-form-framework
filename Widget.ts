// jIMU (WAB) imports:
/// <amd-dependency path="jimu/BaseWidget" name="BaseWidget" />
declare var BaseWidget: any; // there is no ts definition of BaseWidget (yet!)
// declareDecorator - to enable us to export this module with Dojo's "declare()" syntax so WAB can load it:
import declare from './support/declareDecorator';
import { FormView, FormFields } from './view/FormView'

// esri imports:
import EsriMap from 'esri/map';

// dojo imports:
// import on from 'dojo/on';

import IConfig from './config';


interface IWidget {
  baseClass: string;
  config?: IConfig;
}

@declare(BaseWidget)
class Widget implements IWidget {
  public baseClass: string = 'SAPAssetEditor';
  public config: IConfig;
  private formView: FormView
  private map: EsriMap;

  private configureWidget() : void {
    const testFormFields: FormFields = [
      {
        fieldName: 'key',
        value: 'value',
      },
    ]
    this.formView = new FormView('SAPWidgetPanel', 'form', testFormFields);
  }
  private postCreate(args: any): void {
    const self: any = this;
    self.inherited(arguments);
    console.log('SAPAssetEditor::postCreate');

  }
  private startup(): void {
    // tslint:disable-next-line: prefer-const
    let self: any = this;
    self.inherited(arguments);
    console.log('SAPAssetEditor::startup');
  };

  private onOpen(): void {
    console.log('SAPAssetEditor::onOpen');
    this.configureWidget();
    // Present Modal View
  };

  // private onClose(): void {
  //   console.log('SAPAssetEditor::onClose');
  // };
  // private onMinimize(): void {
  //   console.log('SAPAssetEditor::onMinimize');
  // };
  // private onMaximize(): void {
  //   console.log('SAPAssetEditor::onMaximize');
  // };
  // private onSignIn(credential): void {
  //   console.log('SAPAssetEditor::onSignIn', credential);
  // };
  // private onSignOut(): void {
  //   console.log('SAPAssetEditor::onSignOut');
  // };
  // private onPositionChange(): void {
  //   console.log('SAPAssetEditor::onPositionChange');
  // };
  // private resize(): void {
  //   console.log('SAPAssetEditor::resize');
  // };
}

export = Widget;