export class WidgetView {
    widget : HTMLElement;

    constructor(widgetId: string) {
        this.widget = document.getElementById(widgetId);
    }

    __getElement(type: HTMLIdentifierTypes, identifier: string, classNumber?: number) : Element {
        if(type === HTMLIdentifierTypes.id) {
            return document.getElementById(identifier);
        } else if(type === HTMLIdentifierTypes.name) {
            return this.widget.getElementsByTagName(identifier)[classNumber || 0];
        } else {
            return this.widget.getElementsByClassName(identifier)[classNumber || 0];
        }
    }
}

export enum HTMLIdentifierTypes{ id, class, name }