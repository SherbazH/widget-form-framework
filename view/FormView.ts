/*
    Form View
    Author: Sherbaz Hashmi
    Email: sherbaz.hashmi@broadspectrum.com
    Version: 1.0.0
*/

import { WidgetView, HTMLIdentifierTypes } from './WidgetView'


class FormView extends WidgetView {
    formStates: [FormFields] // Storing Form Field States for Future References
    private formInitialised = false; // Flag whether form has be initialised with default values
    private formClassName : string;
    private formContainer : HTMLDivElement;

    constructor(widgetId: string, formClassName: string, formFields: FormFields) {
        super(widgetId);
        this.formStates = [ formFields ];
        this.generateForm();
        this.formClassName = formClassName;
        this.formInitialised = true;
    }

    public getState(): FormFields {
        if(!this.formExists() && this.formInitialised) {
            throw new Error('Form does not exist!');
        }

        const lastFormStateIndex = this.formStates.length - 1;

        if(lastFormStateIndex < 0) {
            throw new Error('Unable to generate form without any fields')
        }

        const newState: FormFields = this.formStates[lastFormStateIndex];

        const fieldContainerClassName = 'field_container';
        const formFieldContainers = this.widget.getElementsByClassName(fieldContainerClassName) as any;

        this.formStates[lastFormStateIndex].forEach((formField: FormField) => {
            // Get FieldContainer
            const fieldContainer = formFieldContainers.filter((element: HTMLElement) => element.className.includes(formField.fieldName))[0];

            // Get Field Input
            const currentFormField : FormField = this.getFormField(fieldContainer);

            // Save in State
            newState[currentFormField.fieldName] = currentFormField.value;
        });

        this.formStates.push(newState)
        return newState;
    }

    public setState(fields: FormFields) {
        this.formStates.push(fields)
    }

    private generateForm() {
        // Generate Container
        this.formContainer = document.createElement('div');
        this.formContainer.className = 'form';

        const lastFormStateIndex = this.formStates.length - 1;

        if(lastFormStateIndex < 0) {
            throw new Error('Unable to generate form without any fields')
        }

        // Get Form Fields
        this.formStates[lastFormStateIndex].forEach((formField: FormField) => {
            const { fieldName, value } = formField;
            // Create Input + Label Container
            const fieldContainer = document.createElement('div');
            fieldContainer.className = `field_container ${fieldName}`;

            // Create Label
            this.generateFormElement(FormDOMElement.Span, fieldName, fieldContainer);
            this.setLabel(fieldName, fieldContainer);

            // Create Input
            this.generateFormElement(FormDOMElement.Input, fieldName, fieldContainer);
            this.setInput(fieldName, value, fieldContainer);
            this.formContainer.appendChild(fieldContainer);
        });

        const potentialWidgetPanels = document.getElementsByClassName('SAPWidgetPanel')
        const actualWidgetPanel = potentialWidgetPanels[0]
        actualWidgetPanel.appendChild(this.formContainer);

    }

    private formExists() {
        const children = this.widget.children as any;
        return children.reduce((total: boolean, current: HTMLElement) => total || (current.className === 'form'));
    }

    private setLabel(labelName: string, fieldContainer?: Element) {
        if(!fieldContainer) {
            // Typecasting as any allows iteration with TSC compiler.
            fieldContainer = super.__getElement(HTMLIdentifierTypes.class, `field_container ${labelName}`);
            throw new Error(`Unable to find DOM Input for ${labelName}`);
        }

        const fieldContainerChildren = fieldContainer.children;

        const modifyChild = (child: HTMLElement) => {
            if(child instanceof HTMLSpanElement) {
                child.innerText = labelName;
            }
        }

        this.applyMethodToChildren(fieldContainerChildren, modifyChild);
    }


    private setInput(inputName: string, predefinedValue?: string, fieldContainer?: Element) {
        // Check for placeholder
        if(!fieldContainer) {
            fieldContainer = super.__getElement(HTMLIdentifierTypes.class, `field_container ${inputName}`);
        }

        const fieldContainerChildren = fieldContainer.children;
        const modifyChild = (child: HTMLElement) => {
            if(child instanceof HTMLInputElement) {
                child.placeholder = inputName;
                child.setAttribute('value', predefinedValue || '');
            }
        }
        this.applyMethodToChildren(fieldContainerChildren, modifyChild);
    }


    private applyMethodToChildren(containers: HTMLCollection, modifyHTMLContainer: ((HTMLElement) => void)) {
        for(let i = 0; i < containers.length; i++) {
            const child = containers[i];
            modifyHTMLContainer(child)
        }
    }

    private getFormField(fieldContainer: HTMLDivElement): FormField {
        const fieldContainerChildren = fieldContainer.children as any;
        let formField: FormField;
        fieldContainerChildren.forEach((child: HTMLElement) => {
            if(child.tagName === FormDOMElement[FormDOMElement.Span].toLowerCase()) {
                formField.fieldName = child.textContent;
            } else if(child.tagName === FormDOMElement[FormDOMElement.Input].toLowerCase()) {
                formField.value = (child as HTMLInputElement).value;
            }
        });

        if(formField.fieldName.length === 0 || !formField) {
            throw new Error(`Unable to retrieve form field ${fieldContainer}`);
        }

        return formField;
    }
    private generateFormElement(type: FormDOMElement, fieldName: string, parentNode?: string | Element) {
        if(!parentNode) {
            parentNode = this.formClassName;
        }

        // Check if parent exists
        if(!(parentNode instanceof Element)) {
            const potentialParentNodes = this.widget.getElementsByClassName(parentNode);
            if(potentialParentNodes.length === 0) {
                throw new Error(`Parent element ${parentNode} does not exist, so unable to add [${type}] ${fieldName}`);
            }
            parentNode = potentialParentNodes[0];
        }

        const element = document.createElement(FormDOMElement[type].toLowerCase());
        element.setAttribute('name', fieldName);

        // Assuming this is the only parent node
        parentNode.appendChild(element);
    }
}

type FormField = { fieldName: string, value: string }
type FormFields = [ FormField ]
enum FormDOMElement { Input, Span }

export { FormView, FormFields }