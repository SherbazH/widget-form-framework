define({
  root: {
    _widgetLabel: 'SAP Asset Editor',
    widgetTitle: 'SAP Asset Editor',
    description: 'A Web App Builder Widget to Modify Asset Data Stored in SAP Hanna Databases'
  }
  // add supported locales below:
  // , "zh-cn": true
});
